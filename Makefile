#+-------------------------------------------------------------------------------------------------+
#| Top level makefile.                                                                             |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

#Disable built-in recipes.
.SUFFIXES:

#Enable secondary expansion.
.SECONDEXPANSION:

#By expanding this variable the directory of the current makefile can be obtained. For correct
#results this has to be expanded immediately. This is usually done with simply expanded variables
#(created by using ":=").
CURRENT_DIR = $(shell realpath --relative-to=. $(dir $(lastword $(MAKEFILE_LIST))))

#This function uses the "tr" utility program to convert an string to uppercase.
#Parameters:
#$1 - The string to convert to uppercase.
UPPERCASE = $(shell echo $1 | tr a-z A-Z)

#Location of this makefile.
TOP_LEVEL_DIR := $(CURRENT_DIR)

#General locations.
MAKEFILE_DIR = $(TOP_LEVEL_DIR)/makefiles
BOARD_DIR = $(TOP_LEVEL_DIR)/board
LIB_DIR = $(TOP_LEVEL_DIR)/lib
COMMON_DIR = $(TOP_LEVEL_DIR)/common
SCRIPTS_DIR = $(TOP_LEVEL_DIR)/scripts
APP_DIR = $(TOP_LEVEL_DIR)/app
MENUCONFIG_DIR = $(LIB_DIR)/menuconfig
PICO_SDK = $(LIB_DIR)/pico-sdk
TINYUSB = $(LIB_DIR)/tinyusb
BUILD_DIR = build

#Verbosity control.
ifeq ($V,1)
  Q =
  TRACE_CC =
  TRACE_CXX =
  TRACE_AS =
  TRACE_LD =
  TRACE_AR =
  TRACE_OBJCOPY =
  TRACE_STRIP =
  TRACE_CREATING =
  TRACE_CHECKING =
  TRACE_MKROMFS =
else
  Q = @
  TRACE_CC = @echo CC $<
  TRACE_CXX = @echo CXX $<
  TRACE_AS = @echo AS $<
  TRACE_LD = @echo LD $@
  TRACE_AR = @echo AR $@
  TRACE_OBJCOPY = @echo OBJCOPY $@
  TRACE_STRIP = @echo STRIP $@
  TRACE_CREATING = @echo CREATING $@
  TRACE_CHECKING = @echo CHECKING $<
  TRACE_MKROMFS = @echo MKROMFS $@
endif

#Set generic phony targets.
.PHONY: all
.PHONY: clean
.PHONY: clean-all

#Define the default target before including any other file.
all:

#Include every other component. Please note that inclusion order is important.
include $(MAKEFILE_DIR)/toolchain/Makefile.native-gcc
include $(MAKEFILE_DIR)/toolchain/Makefile.native-g++
include $(MAKEFILE_DIR)/toolchain/Makefile.cross-gcc
include $(MAKEFILE_DIR)/tool/Makefile.flex-bison
include $(MAKEFILE_DIR)/tool/Makefile.menuconfig
include $(MAKEFILE_DIR)/tool/Makefile.elf2uf2
include $(MAKEFILE_DIR)/tool/Makefile.pioasm
include $(MAKEFILE_DIR)/tool/Makefile.python
include $(MAKEFILE_DIR)/Makefile.autoconf
ifdef CONFIG_BOARD
include $(BOARD_DIR)/$(CONFIG_BOARD)/Makefile.board
include $(TOP_LEVEL_DIR)/minimos/Makefile.minimos
include $(APP_DIR)/Makefile.app
include $(TOP_LEVEL_DIR)/minimos/Makefile.romfs
-include $(BOARD_DIR)/$(CONFIG_BOARD)/Makefile.board-target
endif

#This recipe deletes all output files without exception.
clean-all:
	rm -rf $(BUILD_DIR)
