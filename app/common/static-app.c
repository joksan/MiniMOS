//+------------------------------------------------------------------------------------------------+
//| Common code for static applications.                                                           |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stdint.h>

//External reference to the application main function.
int main(int argc, char *argv[]);

//Type definition for the static application header.
typedef struct {
  char magic[4];    //Magic number (used to identify the program type)
  uintptr_t entry;  //Application entry point
} static_app_header_t;

//Static application header instance. Allows the operating system to detect the program type and to
//locate the application entry point.
__attribute__((section(".static_app_header")))
const static_app_header_t static_app_header = {
  .magic = "\x7FMSA",         //MSA stands for MiniMOS Static Application
  .entry = (uintptr_t) main,  //Set the entry point to the application main function
};
