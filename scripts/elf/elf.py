#+-------------------------------------------------------------------------------------------------+
#| Simplistic library for analyzing ELF file contents.                                             |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

import struct, collections

#ELF constants as defined in elf.h (this is only a partial list).
ELFMAG = b'\x7FELF'

ELFCLASS32 = 1

ELFDATA2LSB = 1

ELFOSABI_NONE = 0

ET_EXEC = 2

EM_ARM = 40
EM_RISCV = 243

EV_CURRENT = 1

SHN_UNDEF = 0
SHN_LORESERVE = 0xff00

SHT_NULL = 0
SHT_PROGBITS = 1
SHT_SYMTAB = 2
SHT_STRTAB = 3

SHF_WRITE = 1 << 0
SHF_ALLOC = 1 << 1
SHF_EXECINSTR = 1 << 2

STT_NOTYP = 0
STT_OBJECT = 1
STT_FUNC = 2
STT_SECTION = 3

STN_UNDEF = 0

PT_LOAD = 1

PF_X = 1 << 0
PF_W = 1 << 1
PF_R = 1 << 2

#Class abstracting a parsed ELF file.
class elf:
  def __init__(self, elf_path):
    #Start by opening the ELF file.
    with open(elf_path, 'rb') as elf_file:
      #Define a named tuple for the identification part of the ELF header, then read the structure
      #from the file and convert it to this named tuple type.
      ident_nt = collections.namedtuple('elf_ident', 'magic_number elf_class data version os_abi '
                                                     'os_abi_version padding')
      ident = ident_nt._make(struct.unpack('4s5B7s', elf_file.read(16)))

      #Now process the rest of the ELF header in a similar fasion. Put the identification part
      #inside of it so the header is properly structured.
      header_nt = collections.namedtuple('elf_header', 'e_ident e_type e_machine e_version e_entry '
                                                       'e_phoff e_shoff e_flags e_ehsize '
                                                       'e_phentsize e_phnum e_shentsize e_shnum '
                                                       'e_shstrndx')
      self.header = header_nt._make((ident,) + struct.unpack('2H5I6H', elf_file.read(36)))

      #Perform some basic checks on the header.
      _check_elf_header(self.header)

      #Define a named tuple for program headers.
      program_header_nt = collections.namedtuple('program_header', 'p_type p_offset p_vaddr '
                                                                   'p_paddr p_filesz p_memsz '
                                                                   'p_flags p_align')

      #Seek to the first program header. Then populate the program header list with the contents of
      #all program headers.
      elf_file.seek(self.header.e_phoff)
      self.program_header = tuple(program_header_nt._make(struct.unpack('8I', elf_file.read(32)))
                                  for i in range(self.header.e_phnum))

      #Now, with all program headers loaded, load all segment data from the ELF file.
      # segment_data = []
      # for ph in self.program_header:
      #   elf_file.seek(ph.p_offset)
      #   segment_data.append(elf_file.read(ph.p_filesz))
      # self.segment_data = tuple(segment_data)
      #Note: This portion is commented out as it's not in use. It may be used in the future to read
      #program contents.

      #Define a named tuple for section headers.
      section_header_nt = collections.namedtuple('section_header', 'sh_name sh_type sh_flags '
                                                                   'sh_addr sh_offset sh_size '
                                                                   'sh_link sh_info sh_addralign '
                                                                   'sh_entsize')

      #Seek to the first section header. Then populate the section header list with the contents of
      #all section headers.
      elf_file.seek(self.header.e_shoff)
      self.section_header = tuple(section_header_nt._make(struct.unpack('10I', elf_file.read(40)))
                                  for i in range(self.header.e_shnum))

      #Find the header of the section-header string table and make sure it's the correct type. Then
      #seek to it and read its contents.
      shstrtab_header = self.section_header[self.header.e_shstrndx]
      if shstrtab_header.sh_type != SHT_STRTAB:
        raise ValueError('Wrong section type for section-header string table')
      elf_file.seek(shstrtab_header.sh_offset)
      self.shstrtab_data = elf_file.read(shstrtab_header.sh_size)

      #Note: With the above section now loaded, sections can now be looked up by their name.

      #Define a named tuple for symbol table entries.
      symbol_table_nt = collections.namedtuple('symbol_table', 'st_name st_value st_size st_info '
                                                               'st_other st_shndx')

      #Find the header of the symbol table, verify its type and entry size, seek to it and read its
      #contents.
      symtab_header = self.find_section('.symtab')
      if symtab_header.sh_type != SHT_SYMTAB:
        raise ValueError('Wrong section type for symbol table')
      if symtab_header.sh_entsize != 16:
        raise ValueError('Unsupported entry size for symbol table')
      elf_file.seek(symtab_header.sh_offset)
      self.symbol_table = tuple(symbol_table_nt._make(struct.unpack('3I2B1H', elf_file.read(16)))
                                for i in range(symtab_header.sh_size // 16))

      #Find the header of the string table, verify its type, seek to it and read its contents.
      strtab_header = self.section_header[symtab_header.sh_link]
      if strtab_header.sh_type != SHT_STRTAB:
        raise ValueError('Wrong section type for string table')
      elf_file.seek(strtab_header.sh_offset)
      self.strtab_data = elf_file.read(strtab_header.sh_size)

  #Get the name of the section indicated by its section header entry.
  def get_section_name(self, section_header_entry):
    #Check whether the section has a valid name.
    if section_header_entry.sh_name != SHN_UNDEF:
      #Section has a valid name. Look it up in the section-header string table.
      return _extract_zero_terminated_string(self.shstrtab_data, section_header_entry.sh_name)
    else:
      #Section has no valid name.
      return None

  #Obtain the name of every section as a tuple of strings.
  def get_all_section_names(self):
    return tuple(self.get_section_name(x) for x in self.section_header)

  #Search for the section indicated by its name and return the corresponding section header entry. A
  #ValueError exception is raised in case the name doesn't exist.
  def find_section(self, section_name):
    for section_header_entry in self.section_header:
      if self.get_section_name(section_header_entry) == section_name:
        return section_header_entry
    raise ValueError('Nonexistent section name')

  #Get the name of the symbol indicated by its symbol table entry.
  def get_symbol_name(self, symbol_table_entry):
    #Check whether the symbol is associated with a section.
    if _symbol_type(symbol_table_entry.st_info) == STT_SECTION:
      #Symbol is associated with a section. Check whether the section index is in the reserved
      #range.
      if symbol_table_entry.st_shndx < SHN_LORESERVE:
        #Not in the reserved range. Return the section name.
        return self.get_section_name(self.section_header[symbol_table_entry.st_shndx])
      else:
        return None
    #Check whether the symbol is defined.
    elif symbol_table_entry.st_name != STN_UNDEF:
      #Symbol is defined. Return its name.
      return _extract_zero_terminated_string(self.strtab_data, symbol_table_entry.st_name)
    else:
      return None

  #Obtain the name of every symbol as a tuple of strings.
  def get_all_symbol_names(self):
    return tuple(self.get_symbol_name(x) for x in self.symbol_table)

  #Search for the symbol indicated by its name and return the corresponding symbol table entry. A
  #ValueError exception is raised in case the name doesn't exist.
  def find_symbol(self, symbol_name):
    for symbol_table_entry in self.symbol_table:
      if self.get_symbol_name(symbol_table_entry) == symbol_name:
        return symbol_table_entry
    raise ValueError('Nonexistent symbol name')

  #Get the names of all object and function symbols in a section.
  def get_symbol_names_in_section(self, section_name):
    section_index = self.section_header.index(self.find_section(section_name))
    return tuple(_extract_zero_terminated_string(self.strtab_data, s.st_name)
                 for s in self.symbol_table if s.st_shndx == section_index
                 and _symbol_type(s.st_info) in (STT_OBJECT, STT_FUNC))

  #Obtain the name of the section that contains a given symbol.
  def get_symbol_section_name(self, symbol_name):
    section_header_index = self.find_symbol(symbol_name).st_shndx
    return self.get_section_name(self.section_header[section_header_index])

  #Get the virtual address of a symbol (or a section).
  def get_symbol_address(self, symbol_name):
    return self.find_symbol(symbol_name).st_value

  #Get the size of a symbol.
  def get_symbol_size(self, symbol_name):
    return self.find_symbol(symbol_name).st_size

#Perform basic consistency and compatibility checks on the ELF header. Raise a ValueError exception
#in case of any error.
def _check_elf_header(elf_header):
  #Make sure the magic number is valid.
  if elf_header.e_ident.magic_number != ELFMAG:
    raise ValueError('Wrong magic number')

  #Only 32-bit ELF files are supported.
  if elf_header.e_ident.elf_class != ELFCLASS32:
    raise ValueError('Unsupported ELF class')

  #Only little-endian data ordering is supported.
  if elf_header.e_ident.data != ELFDATA2LSB:
    raise ValueError('Unsupported endianness')

  #Only version 1 format is supported.
  if elf_header.e_ident.version != 1:
    raise ValueError('Unsupported ELF version')

  #No operating system ABI is supported.
  if elf_header.e_ident.os_abi != ELFOSABI_NONE:
    raise ValueError('Wrong OS ABI')

  #Consequently, no OS ABI version should be provided.
  if elf_header.e_ident.os_abi_version != 0:
    raise ValueError('Wrong OS ABI version')

  #Only executable ELF files are supported (no shared libraries and such).
  if elf_header.e_type != ET_EXEC:
    raise ValueError('Wrong ELF type')

  #Our current goal is to support ARM and RISC-V architectures only.
  if elf_header.e_machine != EM_ARM and elf_header.e_machine != EM_RISCV:
    raise ValueError('Unsupported machine')

  #Only version 1 format is supported.
  if elf_header.e_version != EV_CURRENT:
    raise ValueError('Unsupported ELF version')

  #Make sure the section header entry size is the one we know about: sizeof(Elf32_Shdr).
  if elf_header.e_shentsize != 40:
    raise ValueError('Invalid section header entry size')

  #Make sure the program header entry size is the one we know about: sizeof(Elf32_Phdr).
  if elf_header.e_phentsize != 32:
    raise ValueError('Invalid program header entry size')

#Extract a zero-terminated string from a string table and convert it to a python string.
def _extract_zero_terminated_string(string_table, start_offset):
  #Locate the end of the string by looking for a terminating zero after the start offset, then
  #extract the contents as an ASCII string.
  end_offset = start_offset + string_table[start_offset::].find(0)
  return string_table[start_offset: end_offset].decode('ascii')

#Extract the symbol type from the symbol information field.
def _symbol_type(st_info):
  return st_info & 0xF
