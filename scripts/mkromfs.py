#+-------------------------------------------------------------------------------------------------+
#| Utility script for creating ROMFS images.                                                       |
#|                                                                                                 |
#| This script is used to generate the ROM/FLASH filesystem (ROMFS) images that are built into     |
#| MiniMOS.                                                                                        |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

import argparse, tomllib
import util, romfs

#Create an argument parser and use it to parse all program arguments.
parser = argparse.ArgumentParser(description='Creates a ROMFS image that can be embedded in a base '
                                             'operating system image')
parser.add_argument('-t', dest='input_toml', required=True,
                    help='the filename of the .toml file with information about the base image')
parser.add_argument('-o', dest='output_bin', required=True,
                    help='the filename of the binary image to generate')
parser.add_argument('filename_pair', nargs='*', metavar='<dst>:<src>',
                    help='Indicates files that will be included in the filesystem. '
                         'Filenames designated by <src> specify the source location in the '
                         'compilation host filesystem. Filenames designated by <dst> specify the '
                         'destination location in the ROMFS filesystem that will be generated. The '
                         'following limitations apply for <dst>: 1. Must not start with a leading '
                         'slash, as filenames are always referenced from root anyway. 2. Must not '
                         'end with a trailing slash, as only files can be specified and not '
                         'directories. Directories are created automatically based on the '
                         'pathnames anyway.')
args = parser.parse_args()

#Read and parse the base image information file.
with open(args.input_toml, 'rb') as toml_file:
  base_image_info = tomllib.load(toml_file)

#Create an instance of the ROMFS generator class.
romfs_instance = romfs.romfs(base_image_info['romfs_header_address'],
                             base_image_info['romfs_header_length'])

#Add every file specified in the filename pairs.
for p in args.filename_pair:
  romfs_instance.add_file(p)

#Open the output binary, then generate all binary data and write it.
with open(args.output_bin, 'wb') as romfs_bin:
  romfs_bin.write(romfs_instance.generate())
