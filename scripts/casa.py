#+-------------------------------------------------------------------------------------------------+
#| Configuration Aggregator for System Applications (CASA).                                        |
#|                                                                                                 |
#| This script is used to add all applications to the build by reading information from their      |
#| respective .toml files. A single Kconfig file is generated, which allows to configure every     |
#| application individually.                                                                       |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

import argparse, os, re, tomllib
import util

#Load application information from a .toml file, making several verifications and adjustments in the
#process.
#Parameters:
# - toml_filename: The filename of the .toml file to parse and load.
def load_app_info(toml_filename):
  app_info_required_keys = ('id', 'name')
  app_info_member_type = {
    'id': str,
    'name': str,
    'category': str,
    'description': str,
    'type': str,
    'default_build': str,
    'default_filesystem': str,
    'default_path': str,
  }
  app_info = {}

  #Make sure the file exists before attempting to open it.
  if not os.path.exists(toml_filename):
    util.print_error('Error: Application information file not found: {}'.format(toml_filename))
    exit(1)

  #Open the .toml file and parse its data.
  with open(toml_filename, 'rb') as toml_file:
    parsed_data = tomllib.load(toml_file)

  #Make sure required keys are present.
  for key in app_info_required_keys:
    if key not in parsed_data:
      util.print_error('Error: Key \'{}\' not found in {}'.format(key, toml_filename))
      exit(1)

  #Make sure there are no extraneous keys.
  for key in parsed_data:
    if key not in app_info_member_type:
      util.print_error('Error: Extraneous key \'{}\' found in {}'.format(key, toml_filename))
      exit(1)

  #Make sure all keys are of the correct type.
  for key, value in parsed_data.items():
    if not isinstance(value, app_info_member_type[key]):
      util.print_error('Error: Value type for key \'{}\' is not \'{}\' but \'{}\' in {}'
                       .format(key, app_info_member_type[key].__name__, type(value).__name__,
                               toml_filename))
      exit(1)

  #Make sure the 'id' key is formatted correctly.
  if not re.fullmatch('[a-z_][a-z0-9_]*', parsed_data['id']):
    util.print_error('Error: Invalid application ID \'{}\' in {}'
                     .format(parsed_data['id'], toml_filename))
    exit(1)

  #Set the category to 'Uncategorized' in case it's not provided.
  if 'category' not in parsed_data:
    parsed_data['category'] = 'Uncategorized'

  #Check whether the 'type' key was provided.
  if 'type' in parsed_data:
    #Key was provided. Make sure it's valid.
    if parsed_data['type'] not in ('static', 'relocatable'):
      util.print_error('Error: Invalid application type \'{}\' in {}'
                       .format(parsed_data['type'], toml_filename))
      exit(1)
  else:
    #Key no provided. Set the application type to 'relocatable' by default.
    parsed_data['type'] = 'relocatable'

  #Check whether the 'default_build' key was provided.
  if 'default_build' in parsed_data:
    #Key was provided. Make sure it's valid.
    if parsed_data['default_build'] not in ('static', 'relocatable', 'disabled'):
      util.print_error('Error: Invalid value \'{}\' for \'default_build\' in {}'
                       .format(parsed_data['default_build'], toml_filename))
      exit(1)
  else:
    #Key not provided. Set the default build option to be the same as the application type, so it
    #gets built by default.
    parsed_data['default_build'] = parsed_data['type']

  #Relocatable applications cannot be built as static. Make sure this is the case.
  if parsed_data['type'] == 'relocatable' and parsed_data['default_build'] == 'static':
      util.print_error('Error: Inconsistent value \'{}\' for \'default_build\' in {}'
                       .format(parsed_data['default_build'], toml_filename))
      exit(1)

  #Check whether the 'default_filesystem' key was provided.
  if 'default_filesystem' in parsed_data:
    #Key was provided. Make sure it's valid.
    if parsed_data['default_filesystem'] not in ('romfs', 'internal', 'external'):
      util.print_error('Error: Invalid value \'{}\' for \'default_filesystem\' in {}'
                       .format(parsed_data['default_filesystem'], toml_filename))
      exit(1)
  else:
    #Key not provided. Set the default filesystem option to romfs.
    parsed_data['default_filesystem'] = 'romfs'

  #Static applications shouldn't be in filesystems other than romfs. Make sure this is the case.
  if parsed_data['default_build'] == 'static' and parsed_data['default_filesystem'] != 'romfs':
    util.print_error('Error: Inconsistent value \'{}\' for \'default_filesystem\' in {}'
                     .format(parsed_data['default_filesystem'], toml_filename))
    exit(1)

  #Set the default path to bin/<application-id> in case it's not provided.
  if 'default_path' not in parsed_data:
    parsed_data['default_path'] = os.path.join('bin', parsed_data['id'])

  return parsed_data

#Create an argument parser and use it to parse all program arguments.
parser = argparse.ArgumentParser(description='Creates a Kconfig file for applications using .toml '
                                             'files as input')
parser.add_argument('-o', dest='output_kconfig', required=True,
                    help='the filename of the Kconfig file to generate')
parser.add_argument('app_list', nargs='+',
                    help='Application list file. Each is a .toml file that contains applications '
                         'to be searched and made available in the generated Kconfig file.')
args = parser.parse_args()

#Gather all application directories from every application list.
app_dirs = []
for toml_filename in args.app_list:
  #Open each .toml file and parse its data.
  with open(toml_filename, 'rb') as toml_file:
    parsed_data = tomllib.load(toml_file)

  #Make sure the 'app_dirs' key is present.
  if 'app_dirs' not in parsed_data:
    util.print_error('Error: Array \'app_dirs\' not found in {}'.format(toml_filename))
    exit(1)

  #Make sure 'app_dirs' is a list.
  if type(parsed_data['app_dirs']) is not list:
    util.print_error('Error: Value type for key \'app_dirs\' is not \'{}\' but \'{}\' in {}'
                     .format(list.__name__, type(parsed_data['app_dirs']).__name__, toml_filename))
    exit(1)

  #Extend the application directory list with all the directories from the parsed data.
  app_dirs.extend(os.path.join(os.path.dirname(toml_filename), d) for d in parsed_data['app_dirs'])

#Gather application information from all application directories.
app_info = []
for appd in app_dirs:
  #Set the path to the application .toml file to be in <application-directory>/app.toml.
  toml_filename = os.path.join(appd, 'app.toml')

  #Load the application information from the .toml file.
  new_info = load_app_info(toml_filename)

  #Make sure the new application doesn't have a conflicting id with an existing one.
  if new_info['id'] in [info['id'] for info in app_info]:
    util.print_error('Error: Repeated application id \'{}\' in {}'
                     .format(new_info['id'], toml_filename))
    exit(1)

  #Append the .toml file to the application information.
  new_info['toml_filename'] = toml_filename

  #Application information loaded successfully. Append it to the list.
  app_info.append(new_info)

#Consolidate the set of categories for all applications.
categories = set()
for info in app_info:
  #Add the application category to the set. Since sets cannot accept repeated elements, all
  #categories will end up in it exactly once.
  categories.add(info['category'])

#Now generate the Kconfig file.
with open(args.output_kconfig, 'w') as kconfig_file:
  #Generate configurations for all applications in every category.
  for category in sorted(categories):
    #Write the menu header for the category.
    kconfig_file.write('menu "{}"\n'.format(category))

    #Iterate for every application matching this category.
    for info in [i for i in app_info if i['category'] == category]:
      #Write the menu header for the application.
      kconfig_file.write('  menu "{}"\n'.format(info['name']))

      #Use the description as the comment text in case one is provided.
      if 'description' in info:
        # kconfig_file.write('      \n')
        kconfig_file.write('    comment "{}"\n'.format(info['description']))

      #Write the choice header for the build type.
      kconfig_file.write('    choice\n')
      kconfig_file.write('      prompt "Build as"\n')

      #Set the default value according to the default_build value.
      kconfig_file.write('      default APP_{}_{}\n'
                         .format(info['id'].upper(),
                                 'STATIC' if info['default_build'] == 'static' else
                                 'RELOCATABLE' if info['default_build'] == 'relocatable' else
                                 'DISABLED'))

      #If the application type is static, provide a selectable value for that.
      if info['type'] == 'static':
        kconfig_file.write('      config APP_{}_STATIC\n'.format(info['id'].upper()))
        kconfig_file.write('        bool "Static application"\n')

      #Provide selectable values for common build options.
      kconfig_file.write('      config APP_{}_RELOCATABLE\n'.format(info['id'].upper()))
      kconfig_file.write('        bool "Relocatable application"\n')
      kconfig_file.write('      config APP_{}_DISABLED\n'.format(info['id'].upper()))
      kconfig_file.write('        bool "None/disabled"\n')

      #Write the choice footer for the build type.
      kconfig_file.write('    endchoice\n')

      #The following configurations depend on the build type not being disabled.
      kconfig_file.write('    if !APP_{}_DISABLED\n'.format(info['id'].upper()))

      #Write the choice header for the target filesystem.
      kconfig_file.write('      choice\n')
      kconfig_file.write('        prompt "Target filesystem"\n')

      #Set the default value according to the default_filesystem value.
      kconfig_file.write('        default APP_{}_{}\n'
                          .format(info['id'].upper(),
                                  'ROMFS' if info['default_filesystem'] == 'romfs' else
                                  'INTERNAL' if info['default_filesystem'] == 'internal' else
                                  'EXTERNAL'))

      #Provide selectable values for the target filesystem.
      kconfig_file.write('        config APP_{}_ROMFS\n'.format(info['id'].upper()))
      kconfig_file.write('          bool "ROMFS"\n')
      kconfig_file.write('        config APP_{}_INTERNAL\n'.format(info['id'].upper()))
      kconfig_file.write('          bool "Internal"\n')
      kconfig_file.write('          depends on !APP_{}_STATIC\n'.format(info['id'].upper()))
      kconfig_file.write('        config APP_{}_EXTERNAL\n'.format(info['id'].upper()))
      kconfig_file.write('          bool "External"\n')
      kconfig_file.write('          depends on !APP_{}_STATIC\n'.format(info['id'].upper()))

      #Write the choice footer for the target filesystem.
      kconfig_file.write('      endchoice\n')

      #Set the application target path within the filesystem.
      kconfig_file.write('      config APP_{}_DST_DIR\n'.format(info['id'].upper()))
      kconfig_file.write('        string "Target path"\n')
      kconfig_file.write('        default "{}"\n'.format(info['default_path']))
      kconfig_file.write('        depends on !APP_{}_EXTERNAL\n'.format(info['id'].upper()))

      #Set the application directory to be the same as where the .toml file is.
      kconfig_file.write('      config APP_{}_SRC_DIR\n'.format(info['id'].upper()))
      kconfig_file.write('        string\n')
      kconfig_file.write('        default "{}"\n'.format(os.path.dirname(info['toml_filename'])))

      #Write the footer for the condition on application being enabled.
      kconfig_file.write('    endif\n')

      #Write the menu footer for the application.
      kconfig_file.write('  endmenu\n')

    #Write the menu footer for the category.
    kconfig_file.write('endmenu\n')

  #Write the consolidated application list as a string.
  kconfig_file.write('config APPS\n')
  kconfig_file.write('  string\n')
  kconfig_file.write('  default "{}"\n'.format(' '.join(info['id'] for info in app_info)))

  #Write the consolidated application information filename list as a string.
  kconfig_file.write('config APP_INFO\n')
  kconfig_file.write('  string\n')
  kconfig_file.write('  default "{}"\n'
                     .format(' '.join(info['toml_filename'] for info in app_info)))
