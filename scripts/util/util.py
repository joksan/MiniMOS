#+-------------------------------------------------------------------------------------------------+
#| Miscellaneous utility functions.                                                                |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

import sys

#Print an error message to stderr.
#Parameters:
# - args: Positional arguments. Passed as-is to the print function.
# - kwargs: Keyworded arguments. Passed as-is as well.
def print_error(*args, **kwargs):
  print(*args, file=sys.stderr, **kwargs)

#Align an address to the next 32-bit word boundary.
#Parameters:
# - address: The address to align.
#Return value: The aligned address.
def align(address):
  return (address + 3) & 0xFFFFFFFC

#Generate as many null bytes as needed to align an address to the next 32-bit word boundary.
#Parameters:
# - address: The potentially unaligned input address.
#Return value: A bytes object containing as many null bytes as needed to align the address.
def pad(address):
  return b'\x00' * (align(address) - address)
