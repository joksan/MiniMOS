#+-------------------------------------------------------------------------------------------------+
#| Sequential Allocator for Linking Static Applications (SALSA).                                   |
#|                                                                                                 |
#| This script is used to allocate memory for every static application stored in the ROM/FLASH     |
#| filesystem (ROMFS). It receives a .toml file containing information about the the initial       |
#| MiniMOS elf file without ROMFS (base image) and one binary file for every static application    |
#| that precedes the one to be linked next. It then calculates the location within the ROMFS area  |
#| of the base image where this new static application can be allocated and proceeds to generate a |
#| linker script that describes this location. This allows the static application to run from its  |
#| designated location without the need of dynamic linking.                                        |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

import argparse, os, tomllib
import util

#Create an argument parser and use it to parse all program arguments.
parser = argparse.ArgumentParser(description='Creates a linker script that can be used to link a '
                                             'static application')
parser.add_argument('-t', dest='input_toml', required=True,
                    help='the filename of the .toml file with information about the base image')
parser.add_argument('-o', dest='output_ld', required=True,
                    help='the filename of the linker script to generate')
parser.add_argument('preceding_binary', nargs='*',
                    help='Static application binary. Indicates which static applications precede '
                         'the one that will be linked next. This is empty for the first static '
                         'application.')
args = parser.parse_args()

#Read and parse the base image information file.
with open(args.input_toml, 'rb') as toml_file:
  base_image_info = tomllib.load(toml_file)

#Get the address of the space that follows the ROMFS header. This will be the origin from where the
#new static application will be allocated. Make sure to align it afterwards.
origin = base_image_info['romfs_header_address'] + base_image_info['romfs_header_length']
origin = util.align(origin)

#Advance the origin for every preceding static application binary. Always keep it aligned.
for binary_filename in args.preceding_binary:
  origin += os.path.getsize(binary_filename)
  origin = util.align(origin)

#Make sure the resulting origin is within the allowed ROM/FLASH range.
if origin >= base_image_info['top_address']:
  util.print_error('Error: Not enough space for allocating more static applications')
  exit(1)

#Calculate the length of the remaining ROM/FLASH memory.
length = base_image_info['top_address'] - origin

#Format the contents of the new linker script to generate.
LD_CONTENTS = '''\
MEMORY {{
  ROM (rx) : ORIGIN = 0x{:X}, LENGTH = 0x{:X}
}}
'''.format(origin, length)

#Generate the linker script now.
with open(args.output_ld, "w") as output_ld:
  output_ld.write(LD_CONTENTS)
