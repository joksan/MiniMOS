#+-------------------------------------------------------------------------------------------------+
#| Library for generating ROMFS images.                                                            |
#|                                                                                                 |
#| This library generates ROMFS filesystem images in a very specific sequence, trying to preserve  |
#| every file - in particular static applications - in its intended location while also allocating |
#| every filesystem object in such a way so as to make sure its location is known before being     |
#| referenced. The process is as follows:                                                          |
#| - First, the client program calls the add_file function for every file that will go in the      |
#|   image. This function immediately reads the file from the compilation host filesystem and      |
#|   appends its contents into the binary data. It then proceeds to record other details such as   |
#|   its name, address and size for later use in the next step. During this step the tree          |
#|   structure of the filesystem is assembled incrementally as each file is checked in.            |
#| - Second, the client program calls the generate function, which performs the following actions: |
#|   - Add every name string to the binary data, recording every address.                          |
#|   - Recursively add all filesystem structures (descriptors and directories) to the binary data, |
#|     using the information collected so far.                                                     |
#|   - Prepend the header to create a finalized image and return it.                               |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

import struct, collections
import util

#Format strings for use with struct library.
_header_fmt = 'I'       #Filesystem header
_descriptor_fmt = 'Ii'  #File/directory content descriptor
_directory_fmt = '2I'   #Directory entry

#Debugging information can be shown by changing _dbg_print to 'print'.
_dbg_print = lambda *args: None
# _dbg_print = print

#Class abstracting the ROMFS generator.
class romfs:
  #Class initialization function.
  #Parameters:
  # - base_addr: The memory adress inside the target device where the ROMFS filesystem structure
  #              is being allocated.
  # - header_len: The size of the header. Used for consistency checking of the header.
  def __init__(self, base_addr, header_len):
    #Start by validating the header length.
    if header_len != len(struct.pack(_header_fmt, 0)):
      raise ValueError('Unexpected header size: {}'.format(header_len))

    #Initialize current memory address to be just ahead of the filesystem header (its content will
    #be determined at a later time). This is where the next piece of data will be placed.
    self.addr = base_addr + header_len

    #Iniatialize the root filesystem structure with an empty directory. Note that subdirectories are
    #kept separate from regular files in order to ease nesting and recursion.
    self.root = {'subdir': [], 'file': []}

    #Start with an empty set of unique filesystem names and an empty dictionary of name string
    #addresses. The name set stores the name string of every file and directory exactly once,
    #whereas the name address dictionary stores and correlates every name with its address.
    self.name_set = set()
    self.name_addr = {}

    #Start with an empty array of binary data. This will be populated during image generation.
    self.bin_data = bytearray()

  #Add a file to the filesystem.
  #Parameters:
  # - filename_pair: A string in the format <destination>:<source>, where <source> is the pathname
  #   of the file in the compilation host filesystem and <destination> is the pathname of the same
  #   file in the ROMFS filesystem to be generated. Note that <destination> has the following
  #   limitations:
  #   - Must not start with a leading slash (paths are always referenced from root anyway).
  #   - Must not end with a trailing slash. That is, only files can be added, not directories. Note
  #     that directories are created automatically based on the pathnames of the files anyway.
  def add_file(self, filename_pair):
    #Start by splitting the filename pair using the colon as a delimiter.
    dst_path, src_path = filename_pair.split(':')

    #Split the destination path into a list of separately named components.
    dst_list = dst_path.split('/')

    #Make sure the path is well formed (e.g. no leading, trailing or duplicate slashes).
    if '' in dst_list:
      raise ValueError('Malformed destination path: {}'.format(dst_path))

    #Read the source file.
    with open(src_path, 'rb') as src:
      file_data = src.read()

    #Create a new dictionary describing the file.
    new_file = {'name': dst_list[-1],     #The name of the file, whichever path it is in
                'addr': self.addr,        #The (current) address inside the binary data
                'size': len(file_data)}   #The amount of bytes just read

    _dbg_print(_fmt_file(new_file))

    #Add the file data to the binary, then align.
    _add_align_bin_data(self, file_data)

    #Update the name set with the destination path list. Since sets cannot accept repeated elements,
    #all file and directory names will end up in it exactly once.
    self.name_set.update(dst_list)

    #Check the new file into the root directory structure. This automatically creates subdirectories
    #for the destination path in case any of them don't exist already. Exclude the name of the file
    #in the third argument, as it mustn't go there and is included in new_file anyway.
    _check_in_file(self.root, new_file, dst_list[:-1])

  #Finalize the filesystem creation process and create the binary data.
  #Return value: A bytes object with the contents of the generated ROMFS image.
  def generate(self):
    #Iterate all strings from the name set. Add each string and its address to the name address
    #dictionary for later access and then add the string to the binary data.
    for n in self.name_set:
      self.name_addr[n] = self.addr
      _add_bin_data(self, n.encode('utf-8') + b'\0')

    #Pad and align the contents of the binary data after adding the last string.
    _align_bin_data(self)

    _dbg_print(_fmt_name_addr(self.name_addr))

    #Add all filesystem structures (descriptors and directories) to the binary data.
    root_desc_addr = _add_filesystem_structures(self, self.root)

    #Generate the ROMFS header, prepend it the to the binary data and return it all.
    return struct.pack(_header_fmt, root_desc_addr) + self.bin_data

#Add binary data to a given romfs object.
def _add_bin_data(romfs_obj, new_data):
  #Extend current binary data with the new data, then advance the address.
  romfs_obj.bin_data.extend(new_data)
  romfs_obj.addr += len(new_data)

#Apply padding to the binary data of a given romfs object in order to align its address to the next
#32-bit word boundary.
def _align_bin_data(romfs_obj):
  romfs_obj.bin_data.extend(util.pad(romfs_obj.addr))
  romfs_obj.addr = util.align(romfs_obj.addr)

#Add binary data to a given romfs object, then apply padding to align it.
def _add_align_bin_data(romfs_obj, new_data):
  _add_bin_data(romfs_obj, new_data)
  _align_bin_data(romfs_obj)

#Register a new file in the destination directory at the relative path. Recursively create parent
#directories if any of them don't exist.
#Parameters:
# - dst_dir: A nested data structure describing the directory where the file will be added.
# - new_file: A dictionary describing the new file to be added.
# - rel_dst: A list of strings describing the relative path where the file is to be registered
#            (excluding the filename at the end).
def _check_in_file(dst_dir, new_file, rel_dst):
  #Check whether there are subdirectories in in the relative destination path.
  if len(rel_dst) == 0:
    #No more subdirectories in the path. Register the new file in this directory.
    dst_dir['file'].append(new_file)
  else:
    #Additional subdirectories remain in the path. Look for existing subdirectories.
    for d in dst_dir['subdir']:
      #Check whether this directory name matches the head of the relative path.
      if d['name'] == rel_dst[0]:
        #Name matches. Recursively register the file in this directory.
        _check_in_file(d, new_file, rel_dst[1:])
        return

    #No matching subdirectory found. Append a new subdirectory with a name equal to the head of the
    #relative path, then recursively register the file in this directory.
    new_dir = {'name': rel_dst[0], 'subdir': [], 'file': []}
    dst_dir['subdir'].append(new_dir)
    _check_in_file(new_dir, new_file, rel_dst[1:])

#Recursively add all filesystem structures (descriptors and directories) to the binary data of a
#romfs object.
#Parameters:
# - romfs_obj: The romfs object to which structures will be added.
# - directory: A nested data structure describing the directory hierarchy to add as binary data.
#Return value: The address of the descriptor generated for the directory.
def _add_filesystem_structures(romfs_obj, directory):
  #This list stores information temporarily so it can be used to fully create the binary data of the
  #directory once it's completed.
  directory_data = []

  #Iterate over subdirectories.
  for s in directory['subdir']:
    #Start by adding subdirectory data (recurse first).
    subdir_desc_addr = _add_filesystem_structures(romfs_obj, s)

    #Append the information regarding this subdirectory to the directory data. The address of the
    #name is obtained by looking it up in the corresponding dictionary.
    directory_data.append({'p_name': romfs_obj.name_addr[s['name']],
                           'p_descriptor': subdir_desc_addr})

  #Note: By recursing first, the directories are assembled from the bottom up, effectively creating
  #the directory structure in reverse tree order. This way all child structures are allocated first
  #and therefore their locations are fully determined before they are referenced by their parent
  #structures (no backtracking needed).

  #Iterate over file entries.
  for f in directory['file']:
    #Append the information regarding this file to the directory data. Look up the address of the
    #name and use the current address, as this descriptor will be added next.
    directory_data.append({'p_name': romfs_obj.name_addr[f['name']],
                           'p_descriptor': romfs_obj.addr})

    _dbg_print(_fmt_descriptor(romfs_obj.addr, f['addr'], f['size']))

    #Add the descriptor for this file to the binary, then align it.
    _add_align_bin_data(romfs_obj, struct.pack(_descriptor_fmt, f['addr'], f['size']))

  #A new directory is about to be added. Get the current address.

  #Keep the current address, then iterate over all directory entries accumulated up to this point.
  directory_addr = romfs_obj.addr
  for d in directory_data:
    #Add every entry to the binary data. Keep data aligned.
    _add_align_bin_data(romfs_obj, struct.pack(_directory_fmt, d['p_name'], d['p_descriptor']))

  _dbg_print(_fmt_directory_data(directory_addr, directory_data))

  #Keep the current address, then generate and add the descriptor for this directory. Align
  #afterwards.
  descriptor_addr = romfs_obj.addr
  _add_align_bin_data(romfs_obj, struct.pack(_descriptor_fmt, directory_addr, -len(directory_data)))

  _dbg_print(_fmt_descriptor(descriptor_addr, directory_addr, -len(directory_data)))

  return descriptor_addr

#Functions for formatting debug information.
#-------------------------------------------

def _fmt_file(file_dict):
  return 'FILE       @ 0x{0:08X}: name = \'{1}\', size = 0x{2:08X} ({2})'\
    .format(file_dict['addr'], file_dict['name'], file_dict['size'])

def _fmt_name_addr(name_addr):
  return '\n'.join('NAME       @ 0x{0:08X}: value = \'{1}\', size = 0x{2:08X} ({2})'\
         .format(v, k, len(k) + 1) for k, v in name_addr.items())

def _fmt_descriptor(address, p_data, size):
  return 'DESCRIPTOR @ 0x{:08X}: p_data = 0x{:08X}, size = 0x{:08X} ({})'\
          .format(address, p_data, size & 0xFFFFFFFF, size)

def _fmt_directory_data(address, directory_data):
  return 'DIRECTORY  @ 0x{:08X}: '.format(address) +\
         ('\n' + ' '*25).join('p_name = 0x{:08X}, p_descriptor = 0x{:08X}'
                              .format(e['p_name'], e['p_descriptor']) for e in directory_data)
