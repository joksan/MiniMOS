#+-------------------------------------------------------------------------------------------------+
#| Static Application Content Observer (SACO).                                                     |
#|                                                                                                 |
#| This script performs several sanity checks in order to ensure that a static application will    |
#| work correctly when run.                                                                        |
#|                                                                                                 |
#| Most checks center around the ideas that static applications must run directly from ROM/FLASH,  |
#| require no relocation and be as small as possible. Also, they must not use statically allocated |
#| RAM, as doing so would imply this memory should always be available for them at a fixed place   |
#| in case they are run (posing further problems which are outside the scope of this document).    |
#| Consequently, the checks are:                                                                   |
#| For the ELF program headers:                                                                    |
#| - There should be no writable segments (they can only be readable and executable).              |
#| - The program must fit in a single LOADable segment (multiple segments may imply gaps or        |
#|   separate memory regions, which will make binaries unnecessarily large).                       |
#| For the ELF sections:                                                                           |
#| - There should be no writable sections. This excludes sections such as .data and .bss.          |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

import argparse
import util, elf

#Create an argument parser and use it to parse all program arguments.
parser = argparse.ArgumentParser(description='Performs sanity checks on a static application image')
parser.add_argument('input_elf',
                    help='the filename of the static application image that is to be cheked')
args = parser.parse_args()

#Load the static application image.
st_app_elf = elf.elf(args.input_elf)

#Check every program header.
for p in st_app_elf.program_header:
  #Make sure there are no writable segments.
  if p.p_flags & elf.PF_W:
    util.print_error('Error: Writable program segment found in {}'.format(args.input_elf))
    exit(1)

  #Make sure the program doesn't rely on unsupported features by means of other section types.
  if p.p_type != elf.PT_LOAD:
    util.print_error('Error: Extraneous program segment type found in {}'.format(args.input_elf))
    exit(1)

#Make sure the program is stored in a single segment.
if len(st_app_elf.program_header) > 1:
  util.print_error('Error: {} contains more than one program header'.format(args.input_elf))
  exit(1)

#Check every section header.
for s in st_app_elf.section_header:
  #Make sure there are no writable sections.
  if s.sh_flags & elf.SHF_WRITE:
    util.print_error('Error: Writable section found in {}'.format(args.input_elf))
    exit(1)
