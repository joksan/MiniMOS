#+-------------------------------------------------------------------------------------------------+
#| Content Analizer for MiniMOS Base Image Objects (CAMBIO).                                       |
#|                                                                                                 |
#| This script is used to perform several consistency checks on an unmodified MiniMOS elf file     |
#| (base image) and then extract useful information from it, such as the location and size of      |
#| specific symbols. This information is stored as a .toml file, which can be used by other tools  |
#| in order to perform changes on it (such as adding static programs or adding the ROMFS image).   |
#| Additional information is also passed as command line arguments to be included directly into    |
#| this .toml file.                                                                                |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

import argparse
import util, elf

#Create an argument parser and use it to parse all program arguments.
parser = argparse.ArgumentParser(description='Analizes a MiniMOS base image for consistency and '
                                             'creates a .toml file with information about it')
parser.add_argument('-b', dest='base_elf', required=True,
                    help='the filename of the base operating system image (without ROMFS)')
parser.add_argument('-o', dest='output_toml', required=True,
                    help='the filename of the information file to generate')
parser.add_argument('-t', dest='top_address', required=True, type=lambda x: int(x, 16),
                    help='the highest usable address of the ROM/FLASH memory space (exclusive)')
parser.add_argument('-s', dest='romfs_section', required=True,
                    help='the section in the base operating system image that contains the ROMFS')
parser.add_argument('-n', dest='romfs_header', required=True,
                    help='the name of the symbol (struct) that holds the ROMFS header')
args = parser.parse_args()

#Load the base image.
base_elf = elf.elf(args.base_elf)

#Make sure there is a ROMFS section.
if args.romfs_section not in base_elf.get_all_section_names():
  util.print_error('Error: No section {} in base image'.format(args.romfs_section))
  exit(1)

#Make sure the ROMFS header is in the elf file.
if args.romfs_header not in base_elf.get_all_symbol_names():
  util.print_error('Error: No symbol {} in base image'.format(args.romfs_header))
  exit(1)

#Make sure the ROMFS header is in the ROMFS section.
if base_elf.get_symbol_section_name(args.romfs_header) != args.romfs_section:
  util.print_error('Error: Symbol {} not in {} section'
                   .format(args.romfs_header, args.romfs_section))
  exit(1)

#Make sure the ROMFS header is the only symbol in the ROMFS section.
if base_elf.get_symbol_names_in_section(args.romfs_section) != (args.romfs_header,):
  util.print_error('Error: Too many symbols in {} section'.format(args.romfs_section))
  exit(1)

#Generate the .toml file now.
with open(args.output_toml, 'w') as toml_file:
  toml_file.write('romfs_header_address = 0x{:08X}\n'
                  .format(base_elf.get_symbol_address(args.romfs_header)))
  toml_file.write('romfs_header_length = {}\n'.format(base_elf.get_symbol_size(args.romfs_header)))
  toml_file.write('top_address = 0x{:08X}\n'.format(args.top_address))
