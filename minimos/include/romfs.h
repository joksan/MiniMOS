//+------------------------------------------------------------------------------------------------+
//| Data structures for the ROM/FLASH filesystem (ROMFS).                                          |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef ROMFS_H_
#define ROMFS_H_

#include <stdint.h>

//Forward declarations of all structures.
typedef struct romfs_header romfs_header_t;
typedef struct romfs_descriptor romfs_descriptor_t;
typedef struct romfs_directory romfs_directory_t;

//Filesystem header. Indicates the location of the root directory.
struct romfs_header {
  const romfs_descriptor_t *p_root_dir;
};

//Filesystem descriptor. Indicates the location and size of the contents of a file or directory.
struct romfs_descriptor {
  union {
    uint8_t *p_file_data;             //File data
    romfs_directory_t *p_directory;   //Directory data
  };
  int32_t size;   //Element size. If zero or positive the element is a file and its size is in
                  //bytes. If negative the element is a directory and its size is the negative of
                  //the amount of entries (-1 means 1 entry, -2 means 2 entries and so on).
};

//Directory entry. A directory can contain one or more of these. Contains the name of a file or
//subdirectory and points to its descriptor.
struct romfs_directory {
  const char *p_name;
  const romfs_descriptor_t *p_descriptor;
};

#endif //ROMFS_H_
