//+------------------------------------------------------------------------------------------------+
//| MiniMOS standard stream API.                                                                   |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef MINIMOS_STD_STREAM_H_
#define MINIMOS_STD_STREAM_H_

#include <stddef.h>

//Type definitions for the functions exposed by an standard stream driver.
typedef int (*minimos_std_stream_available_t)(void);
typedef int (*minimos_std_stream_read_t)(void *p_buf, size_t len);
typedef int (*minimos_std_stream_write_t)(const void *p_buf, size_t len);

//Type definition for an standard stream driver struct.
typedef struct minimos_std_stream_s {
  struct minimos_std_stream_s *p_next;
  const minimos_std_stream_available_t available;
  const minimos_std_stream_read_t read;
  const minimos_std_stream_write_t write;
} minimos_std_stream_t;

//Exposed functions.
void minimos_std_stream_add(minimos_std_stream_t *p_stream);
int minimos_std_stream_available(void);
int minimos_std_stream_read(void *p_buf, size_t len);
int minimos_std_stream_write(const void *p_buf, size_t len);

#endif //MINIMOS_STD_STREAM_H_
