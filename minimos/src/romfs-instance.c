//+------------------------------------------------------------------------------------------------+
//| Unique instance of the ROM/FLASH filesystem (ROMFS).                                           |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stddef.h>
#include "romfs.h"

//Declare the filesystem header here, allocating it in its own special section. Note that the actual
//contents of the filesystem are populated after linking the base operating system, so this header
//is essentially empty and therefore no root directory is defined. The location in memory of this
//header (which is expected to be at the end of the operating system image) is used as the base
//address to allocate all filesystem contents.
__attribute__((section(".romfs")))
const romfs_header_t romfs_header = {
  .p_root_dir = NULL,
};
