//+------------------------------------------------------------------------------------------------+
//| Entry point for the operating system portion of MiniMOS.                                       |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include "board.h"
#include "qvga-video.h"

//This is just an stub for now.
int main(void) {
  //Initialize all components.
  board_init();
  qvga_video_init();

  for (;;);
}
