//+------------------------------------------------------------------------------------------------+
//| System calls implementation for MiniMOS.                                                       |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stddef.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <minimos-std-stream.h>

//Write on a file.
//Parameters:
//- file: File descriptor.
//- buf: Source buffer.
//- nbyte: Amount of bytes to write.
//Return value: The number of bytes actually written to the file (which may be less than requested).
//-1 otherwise (sets errno to indicate the error).
int _write(int file, const void *buf, size_t nbyte) {
  switch (file) {
    case STDOUT_FILENO:
    case STDERR_FILENO:   //Consolidate stderr with stdout
      return minimos_std_stream_write(buf, nbyte);
    default:
      errno = EBADF;
      return -1;
  }
}

//Read from a file.
//Parameters:
//- file: File descriptor.
//- buf: Destination buffer.
//- nbyte: Amount of bytes to read.
//Return value: A non negative value to indicate a successful operation (which may be less than
//requested). -1 otherwise (sets errno to indicate the error).
int _read(int file, void *buf, size_t nbyte) {
  switch (file) {
    case STDIN_FILENO:
      //Wait for any amount of characters to arrive.
      while (minimos_std_stream_available() == 0);

      //Read as many characters as requested or as possible.
      return minimos_std_stream_read(buf, nbyte);
    default:
      errno = EBADF;
      return -1;
  }
}

//Close a file descriptor.
//Parameters:
//- file: File descriptor.
//Return value: 0 if successful. -1 otherwise (sets errno to indicate the error).
int _close(int file) {
  (void) file;

  //Unimplemented. Always return an error.
  errno = EBADF;
  return -1;
}

//Get file status.
//Parameters:
//- file: File descriptor.
//- st: structure where to put information about the file.
//Return value: 0 if successful. -1 otherwise (sets errno to indicate the error).
int _fstat(int file, struct stat *st) {
  switch (file) {
    case STDIN_FILENO:
    case STDOUT_FILENO:
    case STDERR_FILENO:
      //All these are character devices.
      st->st_mode = S_IFCHR;
      return 0;
    default:
      errno = EBADF;
      return -1;
  }
}

//Test whether a file descriptor refers to a terminal.
//Parameters:
//- file: File descriptor.
//Return value: 1 if the file descriptor refers to a terminal. 0 otherwise (sets errno to indicate
//the error).
int _isatty(int file) {
  switch (file) {
    case STDIN_FILENO:
    case STDOUT_FILENO:
    case STDERR_FILENO:
      //All these refer to a terminal.
      return 1;
    default:
      errno = EBADF;
      return 0;
  }
}

//Move the read/write file offset.
//Parameters:
//- file: File descriptor.
//- offset: File offset.
//- whence: Reference position.
//Return value: The resulting offset in bytes, from the beginning of the file.
_off_t _lseek(int file, _off_t offset, int whence) {
  (void) file;
  (void) offset;
  (void) whence;

  //Unimplemented.
  return 0;
}
