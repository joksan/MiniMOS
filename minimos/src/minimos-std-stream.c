//+------------------------------------------------------------------------------------------------+
//| MiniMOS standard stream API.                                                                   |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stddef.h>
#include "minimos-std-stream.h"

//Head and tail of the standard stream driver list.
static minimos_std_stream_t *p_stream_head = NULL;
static minimos_std_stream_t *p_stream_tail = NULL;

//Pointer to the last known stream driver that has input data available.
static minimos_std_stream_t *p_available_stream = NULL;

//Add an standard stream driver to the list.
//Parameters:
//- p_stream: The standard stream driver to add.
void minimos_std_stream_add(minimos_std_stream_t *p_stream) {
  if (p_stream_head == NULL) {
    //List is empty. Set both head and tail to the same item.
    p_stream_head = p_stream;
    p_stream_tail = p_stream;
  }
  else {
    //List has data already. Have the last item point to the new item, then move the tail.
    p_stream_tail->p_next = p_stream;
    p_stream_tail = p_stream;
  }

  //Make sure the list ends in NULL.
  p_stream_tail->p_next = NULL;
}

//Check how many bytes are available in any of the standard input stream drivers.
//Return value: The amount of bytes available.
int minimos_std_stream_available(void) {
  minimos_std_stream_t *p_stream;
  int available_count = 0;

  //Iterate through the driver list.
  for (p_stream = p_stream_head; p_stream != NULL; p_stream = p_stream->p_next) {
    //Check how many bytes are available in each driver.
    available_count = p_stream->available();

    if (available_count > 0) {
      //This driver has available data. Store a reference to it and return the amount.
      p_available_stream = p_stream;
      return available_count;
    }
  }

  return 0;
}

//Perform a read operation on any of the standard input stream drivers. Note that if a driver was
//successfully queried for available data immediately before, then it's used for the read operation.
//Parameters:
//- p_buf: Pointer to destination buffer.
//- len: The requested amount of bytes to read.
//Return value: The amount of bytes actually read (can be less than requested).
int minimos_std_stream_read(void *p_buf, size_t len) {
  int read_count = 0;

  //Check whether no standard stream driver is known to have input data available.
  if (p_available_stream == NULL) {
    //No driver known. Try to locate one.
    minimos_std_stream_available();
  }

  //Check whether an standard stream driver is known to have input data available.
  if (p_available_stream != NULL) {
    //Driver known. Perform the read operation on it.
    read_count = p_available_stream->read(p_buf, len);

    //All done. Assume no known driver from now on.
    p_available_stream = NULL;
  }

  return read_count;
}

//Perform a write operation on all of the standard output stream drivers.
//Parameters:
//- p_buf: Pointer to source buffer.
//- len: The requested amount of bytes to write.
//Return value: The amount of bytes written (always assumed to be the same as requested).
int minimos_std_stream_write(const void *p_buf, size_t len) {
  minimos_std_stream_t *p_stream;

  //Iterate through the driver list.
  for (p_stream = p_stream_head; p_stream != NULL; p_stream = p_stream->p_next) {
    //Perform the write operation on each driver.
    p_stream->write(p_buf, len);
  }

  return len;
}
