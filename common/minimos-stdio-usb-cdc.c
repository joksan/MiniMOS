//+------------------------------------------------------------------------------------------------+
//| USB CDC standard stream driver.                                                                |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include "tusb.h"
#include "board.h"
#include "minimos-std-stream.h"
#include "minimos-stdio-usb-cdc.h"

//Check how many bytes are available in the USB CDC input buffer.
//Return value: The amount of bytes available.
static int usb_cdc_available(void) {
  int available_count = 0;

  //Take the TinyUSB API semaphore.
  board_tiny_usb_api_sem_take();

  //Perform the TinyUSB background task, if needed.
  tud_task();

  //Make sure the USB device is connected.
  if (!tud_cdc_connected()) {
    goto end;
  }

  //All is good. Perform the availability check now.
  available_count = tud_cdc_available();

end:
  //Release the TinyUSB API semaphore.
  board_tiny_usb_api_sem_give();

  return available_count;
}

//Perform a read operation from the USB CDC input buffer.
//Parameters:
//- p_buf: Pointer to destination buffer.
//- len: The requested amount of bytes to read.
//Return value: The amount of bytes actually read (can be less than requested).
static int usb_cdc_read(void *p_buf, size_t len) {
  int read_count = 0;

  //Take the TinyUSB API semaphore.
  board_tiny_usb_api_sem_take();

  //Perform the TinyUSB background task, if needed.
  tud_task();

  //Make sure the USB device is connected.
  if (!tud_cdc_connected()) {
    goto end;
  }

  //All is good. Perform the read operation now.
  read_count = tud_cdc_read(p_buf, len);

end:
  //Release the TinyUSB API semaphore.
  board_tiny_usb_api_sem_give();

  return read_count;
}

//Perform a write operation to the USB CDC output buffer.
//Parameters:
//- p_buf: Pointer to source buffer.
//- len: The requested amount of bytes to write.
//Return value: The amount of bytes actually written (can be less than requested).
static int usb_cdc_write(const void *p_buf, size_t len) {
  size_t tx_len;
  size_t write_count = 0;

  //Take the TinyUSB API semaphore.
  board_tiny_usb_api_sem_take();

  while (write_count < len) {
    //Drop the characters if there's no connection.
    if (!tud_cdc_connected())
      break;

    //Get the maximum amount of characters that can be written at the moment.
    tx_len = tud_cdc_write_available();

    //Limit the transfer size to the remaining amount.
    if (tx_len > len - write_count) {
      tx_len = len - write_count;
    }

    //Write the characters now (if any), then flush the write buffer.
    tud_cdc_write(p_buf + write_count, tx_len);
    tud_cdc_write_flush();

    //Perform the TinyUSB background task, if needed.
    tud_task();

    //Update the write count.
    write_count += tx_len;
  }

  //Release the TinyUSB API semaphore.
  board_tiny_usb_api_sem_give();

  return write_count;
}

//Define the functions for the USB CDC standard stream driver.
static minimos_std_stream_t usb_cdc_stream = {
  .available = usb_cdc_available,
  .read = usb_cdc_read,
  .write = usb_cdc_write,
};

//Initialize the USB CDC standard stream driver.
void minimos_stdio_usb_cdc_init(void) {
  minimos_std_stream_add(&usb_cdc_stream);
}
