//+------------------------------------------------------------------------------------------------+
//| USB CDC descriptors for MiniMOS.                                                               |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include "tusb.h"

#define USB_BCD 0x0200  //USB version 2.0
#define USB_VID 0xCafe  //Vendor ID
#define USB_PID 0x0001  //Device ID

//Device descriptor.
static const tusb_desc_device_t device_descriptor = {
  .bLength            = sizeof(tusb_desc_device_t),
  .bDescriptorType    = TUSB_DESC_DEVICE,
  .bcdUSB             = USB_BCD,

  //Use Interface Association Descriptor (IAD) for CDC. As required by USB Specs IAD's subclass must
  //be common class (2) and protocol must be IAD (1)
  .bDeviceClass       = TUSB_CLASS_MISC,
  .bDeviceSubClass    = MISC_SUBCLASS_COMMON,
  .bDeviceProtocol    = MISC_PROTOCOL_IAD,

  .bMaxPacketSize0    = CFG_TUD_ENDPOINT0_SIZE,

  .idVendor           = USB_VID,
  .idProduct          = USB_PID,
  .bcdDevice          = 0x0100,

  .iManufacturer      = 0x01,
  .iProduct           = 0x02,
  .iSerialNumber      = 0x03,

  .bNumConfigurations = 0x01
};

//Get the USB device descriptor.
//Return value: The pointer to the descriptor data.
uint8_t const *tud_descriptor_device_cb(void) {
  return (uint8_t const *) &device_descriptor;
}

//Enumeration of available interfaces.
enum {
  ITF_NUM_CDC = 0,
  ITF_NUM_CDC_DATA,
  ITF_NUM_TOTAL
};

//Endpoint numbers.
#define EPNUM_CDC_NOTIF   0x81
#define EPNUM_CDC_OUT     0x02
#define EPNUM_CDC_IN      0x82

//Total length of configuration descriptor.
#define CONFIG_TOTAL_LEN    (TUD_CONFIG_DESC_LEN + TUD_CDC_DESC_LEN)

//Configuration descriptor for full speed.
static const uint8_t configuration_descriptor_fs[] = {
  //Config number, interface count, string index, total length, attribute, power in mA.
  TUD_CONFIG_DESCRIPTOR(1, ITF_NUM_TOTAL, 0, CONFIG_TOTAL_LEN, 0x00, 100),

  //Interface number, string index, EP notification address and size, EP data address (out, in) and
  //size.
  TUD_CDC_DESCRIPTOR(ITF_NUM_CDC, 4, EPNUM_CDC_NOTIF, 8, EPNUM_CDC_OUT, EPNUM_CDC_IN, 64),
};

//Get the USB configuration descriptor.
//Parameters:
//- index: Configuration descriptor index.
//Return value: The pointer to the descriptor data.
uint8_t const *tud_descriptor_configuration_cb(uint8_t index) {
  (void) index;   //Unused (only for multiple configurations)

  return configuration_descriptor_fs;
}

//LANGID (language ID) array. This defines the supported languages, is similar to an string
//descriptor and must be at index 0.
static union {
  struct langid_array_s {
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint16_t wLANGID[1];
  } la __attribute__((packed));
  uint16_t u16[sizeof(struct langid_array_s) / sizeof(uint16_t)];
} langid_array = {
  .la = {
    .bLength = sizeof(struct langid_array_s),
    .bDescriptorType = TUSB_DESC_STRING,
    .wLANGID = {
      0x0409,   //English (United States)
    },
  },
};

//Macro used to instantiate an string descriptor. String characters are passed as variadic
//arguments, allowing to group them as array initializers and also to count them.
#define STRING_DESCRIPTOR(name, ...)\
static union {\
  struct name##_s {\
    uint8_t bLength;\
    uint8_t bDescriptorType;\
    uint16_t bString[sizeof((uint16_t []) { __VA_ARGS__ }) / sizeof(uint16_t)];\
  } sd __attribute__((packed));\
  uint16_t u16[sizeof(struct name##_s) / sizeof(uint16_t)];\
} name = {\
  .sd = {\
    .bLength = sizeof(struct name##_s),\
    .bDescriptorType = TUSB_DESC_STRING,\
    .bString = { __VA_ARGS__ },\
  },\
};

//String descriptors.
STRING_DESCRIPTOR(manufacturer_string_descriptor, 'M', 'i', 'n', 'i', 'M', 'O', 'S')
STRING_DESCRIPTOR(product_string_descriptor, 'T', 'e', 's', 't', ' ', 'D', 'e', 'v', 'i', 'c', 'e')
STRING_DESCRIPTOR(serial_number_string_descriptor, '0', '0', '0', '0', '0', '0', '0', '1')

//Get a USB string descriptor.
//Parameters:
//- index: String descriptor index.
//- langid: Language ID.
//Return value: The pointer to the descriptor data.
uint16_t const *tud_descriptor_string_cb(uint8_t index, uint16_t langid) {
  (void) langid;  //Unused (only for multiple languages)

  switch (index) {
    case 0:
      return langid_array.u16;
    case 1:
      return manufacturer_string_descriptor.u16;
    case 2:
      return product_string_descriptor.u16;
    case 3:
      return serial_number_string_descriptor.u16;
    default:
      return NULL;
  }
}
