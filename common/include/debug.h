//+------------------------------------------------------------------------------------------------+
//| Debugging macros for MiniMOS.                                                                  |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef DEBUG_H_
#define DEBUG_H_

//Use RTT for debug output if enabled.
#if DEBUG
#include <SEGGER_RTT.h>
#define DEBUG_PRINTF(fmt, ...) SEGGER_RTT_printf(0, fmt __VA_OPT__(,) __VA_ARGS__)
#else
#define DEBUG_PRINTF(fmt, ...)
#endif

#endif //DEBUG_H_
