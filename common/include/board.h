//+------------------------------------------------------------------------------------------------+
//| Generic board support API.                                                                     |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef BOARD_H_
#define BOARD_H_

//Exposed functions.
void board_init(void);
#if BOARD_USE_TINY_USB
void board_tiny_usb_api_sem_take(void);
void board_tiny_usb_api_sem_give(void);
#endif //BOARD_USE_TINY_USB

#endif //BOARD_H_
