//+------------------------------------------------------------------------------------------------+
//| Generic QVGA support API.                                                                      |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef QVGA_VIDEO_H_
#define QVGA_VIDEO_H_

#include <stdint.h>

//Type definition for the QVGA frame buffer. Every pixel is encoded as a RGB 3-3-2.
typedef union {
  uint8_t u8_ud[320 * 240];         //Access as 8-bit unidimensional
  uint8_t u8_bd[240][320];          //Access as 8-bit bidimensional
  uint32_t u32_ud[320 * 240 / 4];   //Access as 32-bit unidimensional
  uint32_t u32_bd[240][320 / 4];    //Access as 32-bit bidimensional
} frame_buffer_t;

//External declaration for the frame buffer.
extern frame_buffer_t frame_buffer;

//Exposed functions.
void qvga_video_init(void);

#endif //QVGA_VIDEO_H_
