//+------------------------------------------------------------------------------------------------+
//| USB CDC standard stream driver.                                                                |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef MINIMOS_STDIO_USB_CDC_H_
#define MINIMOS_STDIO_USB_CDC_H_

//Exposed functions.
void minimos_stdio_usb_cdc_init(void);

#endif //MINIMOS_STDIO_USB_CDC_H_
