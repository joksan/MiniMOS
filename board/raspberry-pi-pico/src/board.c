//+------------------------------------------------------------------------------------------------+
//| Board support for the Raspberry Pi Pico.                                                       |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include "RP2040.h"
#include "hardware/clocks.h"

#if BOARD_USE_TINY_USB
#include <stdbool.h>
#include "tusb.h"
#endif //BOARD_USE_TINY_USB

#if BOARD_USE_USB_CDC
#include "minimos-stdio-usb-cdc.h"
#endif //BOARD_USE_USB_CDC

#include "board.h"

#if BOARD_USE_TINY_USB
//Semaphore flag used to protect the TinyUSB API from concurrent calls originating from interrupt
//context.
static volatile bool tiny_usb_api_sem_taken = false;
#endif //BOARD_USE_TINY_USB

//Interrupt handler for SysTick timer. This has lower priority than any other IRQ by default
//(assuming interrupt priorities are not set). This is used to handle background tasks by preempting
//the application software.
void SysTick_Handler(void) {
  //Call the TinyUSB background task handler only if an application isn't calling TinyUSB API at the
  //moment.
#if BOARD_USE_USB_CDC
  if (!tiny_usb_api_sem_taken) {
    tud_task();
  }
#endif //BOARD_USE_USB_CDC
}

//Initialize all components for this board.
void board_init(void) {
#if BOARD_USE_TINY_USB
  //Enable the tinyUSB stack.
  tusb_init();
#endif //BOARD_USE_TINY_USB

#if BOARD_USE_USB_CDC
  //Initialize the USB CDC standard stream driver.
  minimos_stdio_usb_cdc_init();
#endif //BOARD_USE_USB_CDC

  //Enable the SysTick timer to handle background tasks.
  SysTick_Config(clock_get_hz(clk_sys)/1000);
  NVIC_EnableIRQ(SysTick_IRQn);
}

#if BOARD_USE_TINY_USB
//Take posession of the TinyUSB API semaphore.
void board_tiny_usb_api_sem_take(void) {
  //Set the semaphore flag with interrupts disabled to ensures that an interrupt won't call the
  //TinyUSB API.
  NVIC_DisableIRQ(SysTick_IRQn);
  tiny_usb_api_sem_taken = true;
  NVIC_EnableIRQ(SysTick_IRQn);
}

//Release posession of the TinyUSB API semaphore.
void board_tiny_usb_api_sem_give(void) {
  tiny_usb_api_sem_taken = false;
}
#endif //BOARD_USE_TINY_USB
