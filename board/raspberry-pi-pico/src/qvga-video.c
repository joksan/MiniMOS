//+------------------------------------------------------------------------------------------------+
//| QVGA display driver.                                                                           |
//|                                                                                                |
//| This module implements VGA video output using 320x240 resolution at 8bpp. I'ts  intended to be |
//| connected to an 8-bit resistor DAC, encoding colors as RGB 3-3-2 (or any other bit             |
//| distribution, actually, but we adhere to that one simply because it's very practical).         |
//|                                                                                                |
//| This module uses PIO and DMA to to drive everything automatically. The user simply needs to    |
//| initialize this module and write to the frame buffer afterwards.                               |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include "RP2040.h"
#include "hardware/pio.h"
#include "hardware/dma.h"
#include "pio-qvga.h"     //Header generated from PIO program
#include "qvga-video.h"

#define PIO_QVGA_INSTANCE pio0
#define PIO_QVGA_V_SYNC_PIN 9
#define PIO_QVGA_H_SYNC_PIN 8
#define PIO_QVGA_RGB_PIN_BASE 0
//Video output pinout:
//9 - VSYNC
//8 - HSYNC
//7 - R2
//6 - R1
//5 - R0
//4 - G2
//3 - G1
//2 - G0
//1 - B1
//0 - B0

//Frame buffer instance.
frame_buffer_t frame_buffer;

//This reserved row is used to draw blank pixels during VSYNC.
static uint32_t blank_row [320 / 4];
//Note: There's a potential alternate implementation where blank pixels are transferred from a
//single location in memory set to constant 0, but changing the length of the transfer would require
//the control block to be twice as big, requiring 525 additional 32-bit words of RAM. This
//implementation requires only 320 / 4 = 80 words of RAM.

//Control block structure for chained DMA transfers.
typedef struct {
  uint32_t *read_addr;
} ctrl_blk_t;

//Array of control blocks. There's one for every line and an extra one for ending the chain.
static ctrl_blk_t ctrl_blk[526];

//State machine indexes.
static int main_sm;   //Moves pixel data and performs HSYNC
static int vsync_sm;  //Performs VSYNC

//DMA channels
static int dma_data_ch;   //Moves pixel data
static int dma_ctrl_ch;   //Controls chained transfers

//Initialize the PIO state machines.
static void initialize_pio(void) {
  uint offset;
  pio_sm_config config;

  //Initialize pin multiplexing.
  pio_gpio_init(PIO_QVGA_INSTANCE, PIO_QVGA_V_SYNC_PIN);
  pio_gpio_init(PIO_QVGA_INSTANCE, PIO_QVGA_H_SYNC_PIN);
  for (int i = 0; i < 8; i++) {
    pio_gpio_init(PIO_QVGA_INSTANCE, PIO_QVGA_RGB_PIN_BASE + i);
  }

  //Configure the main PIO state machine.
  main_sm = pio_claim_unused_sm(PIO_QVGA_INSTANCE, true);
  offset = pio_add_program(PIO_QVGA_INSTANCE, &pio_qvga_main_program);
  config = pio_qvga_main_program_get_default_config(offset);
  sm_config_set_out_pins(&config, PIO_QVGA_RGB_PIN_BASE, 8);
  sm_config_set_set_pins(&config, PIO_QVGA_H_SYNC_PIN, 1);
  sm_config_set_clkdiv_int_frac(&config, 5, 0);
  sm_config_set_out_shift(&config, true, true, 32);
  sm_config_set_fifo_join(&config, PIO_FIFO_JOIN_TX);
  pio_sm_set_consecutive_pindirs(PIO_QVGA_INSTANCE, main_sm, PIO_QVGA_RGB_PIN_BASE, 8, true);
  pio_sm_set_consecutive_pindirs(PIO_QVGA_INSTANCE, main_sm, PIO_QVGA_H_SYNC_PIN, 1, true);
  pio_sm_init(PIO_QVGA_INSTANCE, main_sm, offset, &config);

  //Configure the vsync PIO state machine.
  vsync_sm = pio_claim_unused_sm(PIO_QVGA_INSTANCE, true);
  offset = pio_add_program(PIO_QVGA_INSTANCE, &pio_qvga_vsync_program);
  config = pio_qvga_vsync_program_get_default_config(offset);
  sm_config_set_sideset_pins(&config, PIO_QVGA_V_SYNC_PIN);
  sm_config_set_out_shift(&config, true, true, 32);
  pio_sm_set_consecutive_pindirs(PIO_QVGA_INSTANCE, vsync_sm, PIO_QVGA_V_SYNC_PIN, 1, true);
  pio_sm_init(PIO_QVGA_INSTANCE, vsync_sm, offset, &config);

  //Pass the initial register values to all state machines.
  pio_sm_put(PIO_QVGA_INSTANCE, main_sm, 320 - 1);        //Y = total - loop adjust
  pio_sm_put(PIO_QVGA_INSTANCE, vsync_sm, 10);            //X = front porch
  pio_sm_put(PIO_QVGA_INSTANCE, vsync_sm, 525 - 1 - 2);   //Y = total - loop adjust - vsync pulse

  //Start the state machines.
  pio_set_sm_mask_enabled(PIO_QVGA_INSTANCE, (1 << main_sm) | (1 << vsync_sm), true);
}

//Initialize DMA channels.
static void initialize_dma(void) {
  dma_channel_config config;

  //Clain all DMA channels.
  dma_data_ch = dma_claim_unused_channel(true);
  dma_ctrl_ch = dma_claim_unused_channel(true);

  //Initialize the DMA channel for data transfers.
  config = dma_channel_get_default_config(dma_data_ch);
  channel_config_set_transfer_data_size(&config, DMA_SIZE_32);  //Transfer 32-bit words
  channel_config_set_dreq(&config, DREQ_PIO0_TX0 + main_sm);    //Main SM makes requests
  channel_config_set_chain_to(&config, dma_ctrl_ch);            //Chain to control channel
  channel_config_set_irq_quiet(&config, true);                  //Only interrupt at end of chain
  channel_config_set_read_increment(&config, true);             //Advance to next word
  channel_config_set_write_increment(&config, false);           //Write to same register
  dma_channel_configure(dma_data_ch, &config,
                        &PIO_QVGA_INSTANCE->txf[main_sm],       //Write to main SM TX fifo
                        NULL,                                   //Read address set by control ch.
                        320 / 4,                                //Move an entire line at a time
                        false);                                 //Don't start yet

  //Configure the first set of control blocks to read from the blank row.
  for (int i = 0; i < 45; i++) {
    ctrl_blk[i] = (ctrl_blk_t) { .read_addr = blank_row, };
  }

  //Configure the next set of control blocks to read every line twice (line doubling).
  for (int i = 45; i < 525; i++) {
    ctrl_blk[i] = (ctrl_blk_t) { .read_addr = frame_buffer.u32_bd[(i - 45) / 2], };
  }

  //Configure the last block to read from null to end the chain.
  ctrl_blk[525] = (ctrl_blk_t) { .read_addr = NULL, };

  //Initialize the DMA channel for control transfers. Note that the control block is only 4 bytes
  //long and transfers can be done by only writing to the read-address/trigger register, so no ring
  //transfers are needed.
  config = dma_channel_get_default_config(dma_ctrl_ch);
  channel_config_set_transfer_data_size(&config, DMA_SIZE_32);        //Transfer 32-bit words
  channel_config_set_read_increment(&config, true);                   //Advance to next block
  channel_config_set_write_increment(&config, false);                 //Write to same register
  dma_channel_configure(dma_ctrl_ch, &config,
                        &dma_hw->ch[dma_data_ch].al3_read_addr_trig,  //Write addresses to data ch.
                        ctrl_blk,                                     //Read from control block
                        1,                                            //Transfer single words
                        true);                                        //Start now
}

//Interrupt handler for DMA requests. Triggers after the whole video frame has been drawn.
void DMA_IRQ_0_Handler(void) {
  //Acknowledge the IRQ.
  dma_channel_acknowledge_irq0(dma_data_ch);

  //Start a new chain transfer.
  dma_channel_set_read_addr(dma_ctrl_ch, ctrl_blk, true);

  //Note: This interrupt handler can be hooked to provide a vertical blank interrupt.
}

//Initialize the QVGA video module.
void qvga_video_init(void) {
  //Clear the blank row.
  for (unsigned i = 0; i < sizeof blank_row / sizeof blank_row[0]; i++) {
    blank_row[0] = 0;
  }

  //Clear the frame buffer.
  for (unsigned i = 0; i < sizeof frame_buffer.u32_ud / sizeof frame_buffer.u32_ud[0]; i++) {
    frame_buffer.u32_ud[i] = 0;
  }

  //Enable interrupts.
  dma_channel_set_irq0_enabled(dma_data_ch, true);
  NVIC_EnableIRQ(DMA_IRQ_0_IRQn);

  //Initialize the PIO state machines, then initialize DMA.
  initialize_pio();
  initialize_dma();
}
