#ifndef TUSB_CONFIG_H_
#define TUSB_CONFIG_H_

#if BOARD_USE_USB_CDC
//Configure TinyUSB as a USB CDC device.
#define CFG_TUSB_RHPORT0_MODE OPT_MODE_DEVICE
#define CFG_TUD_CDC 1
#define CFG_TUD_CDC_RX_BUFSIZE 256
#define CFG_TUD_CDC_TX_BUFSIZE 256
#else
#error No valid board configuration given for TinyUSB
#endif //BOARD_USE_USB_CDC

#endif //TUSB_CONFIG_H_
