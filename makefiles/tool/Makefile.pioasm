#+-------------------------------------------------------------------------------------------------+
#| Support for the PIO assembler tool from the Rasbperry Pi Pico SDK.                              |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

#Tool-specific settings.
PIOASM = $(pioasm-output-elf)

#C++ include paths.
pioasm-cpp-includes += $(PICO_SDK)/tools/pioasm
pioasm-cpp-includes += $(PICO_SDK)/tools/pioasm/gen

#PIOASM C++ sources.
pioasm-cpp-sources += $(PICO_SDK)/tools/pioasm/gen/lexer.cpp
pioasm-cpp-sources += $(PICO_SDK)/tools/pioasm/gen/parser.cpp
pioasm-cpp-sources += $(PICO_SDK)/tools/pioasm/main.cpp
pioasm-cpp-sources += $(PICO_SDK)/tools/pioasm/pio_assembler.cpp
pioasm-cpp-sources += $(PICO_SDK)/tools/pioasm/pio_disassembler.cpp
pioasm-cpp-sources += $(PICO_SDK)/tools/pioasm/c_sdk_output.cpp

#Generate target-specific settings and recipes.
$(eval $(call NATIVE_GPP_ADD_TARGET,pioasm))

#Calling and evaluating this function creates a complete set of rules that can be used to assemble
#PIO sources for a target.
#Parameters:
#$1 - The name of the target.
define PIOASM_ADD_TARGET =
#Set the output directory for the target.
$1-output-dir = $$(BUILD_DIR)/$1

#Set all header names from PIO assembler sources.
$1-headers = $$(addprefix $$($1-output-dir)/,$$(notdir $$($1-sources:%.pio=%.h)))

#Set every PIO assembler source as a prerequisite to its corresponding header file.
$$(foreach s,$$($1-sources),$$(eval $$($1-output-dir)/$$(notdir $$(s:%.pio=%.h)): $$(s)))

#This recipe creates the output directory.
$$($1-output-dir):
	mkdir -p $$@

#This assembles all PIO assembler sources. Don't set any prerequisite to this pattern rule, as each
#header will have their prerequisites defined explicitly.
$$($1-headers): %.h: | $$($1-output-dir) $$(PIOASM)
	$$(PIOASM) -o c-sdk $$< $$@

#This removes all output files for the target.
.PHONY: $1-clean
clean: $1-clean
$1-clean:
	rm -rf $$($1-output-dir)

endef #PIOASM_ADD_TARGET
